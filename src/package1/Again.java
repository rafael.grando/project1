package package1;

import java.util.Locale;
import java.util.Scanner;

public class Again {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        System.out.println("diite um numero para descobrir se é impar ou par");
        int x = sc.nextInt();

        if (x%2 == 0) {
            System.out.println("par");
        } else{
            System.out.println("impar");
        }

        sc.close();
    }

}

